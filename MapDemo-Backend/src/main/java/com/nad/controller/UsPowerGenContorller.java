package com.nad.controller;

import com.nad.model.MapMarkerModel;
import com.nad.model.State;
import com.nad.service.UsStatePowerGenService;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@CrossOrigin("*")
public class UsPowerGenContorller {

    @Autowired
    private UsStatePowerGenService usStatePowerGenService;

    @GetMapping("/get-all-markers")
    @ApiOperation(value = "Get All Markers")
    @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public ResponseEntity<List<MapMarkerModel>> getAllMapMarkers() {
        return ResponseEntity.ok(usStatePowerGenService.getMapMarkerModels());
    }

    @GetMapping("/get-top-n-markers")
    @ApiOperation(value = "Get All Markers")
    @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public ResponseEntity<List<MapMarkerModel>> getMapMarkes(@ApiParam(name = "topN") @RequestParam(name = "topN") Integer topN) {
        return ResponseEntity.ok(usStatePowerGenService.getTopNMarkers(topN));

    }
    
    @GetMapping("/get-by-state-markers")
    @ApiOperation(value = "Get All Markers")
    @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public ResponseEntity<List<MapMarkerModel>> getMapMarkerByState(@ApiParam(name = "stateName") @RequestParam(name="stateName") String stateName) {
        return ResponseEntity.ok(usStatePowerGenService.findByStateName(stateName));

    }

    @GetMapping("/get-states")
    @ApiOperation(value = "Get All States List")
    @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public ResponseEntity<List<State>> getStates() {
        return ResponseEntity.ok(usStatePowerGenService.getStates());

    }
}
