package com.nad.service;


import com.nad.entity.UsStatePowerGenEntity;
import com.nad.model.Label;
import com.nad.model.MapMarkerModel;
import com.nad.model.Position;
import com.nad.model.State;
import com.nad.repository.UsStatePowerGenRepository;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsStatePowerGenService {

    private static final DecimalFormat df = new DecimalFormat("0.00");
    @Autowired
    private UsStatePowerGenRepository genRepository;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public List<MapMarkerModel> getMapMarkerModels(){
       return genRepository.findAll().stream().map(us->{
            MapMarkerModel map=new MapMarkerModel();
            map.setPosition(new Position(us.getLat(), us.getLng()));
           // map.setInfo("State Name "+us.getName());
            map.setLabel(new Label("red" , String.valueOf(us.getTotalGen())));
            map.setTitle("Power Generation Details");
            return map;
        }).collect(Collectors.toList());
    }
    
    public List<MapMarkerModel> getTopNMarkers(int n){
        double totalGeneration= genRepository.findAll().stream().mapToDouble(gen->gen.getTotalGen()).sum();
        
       return entityManager.createQuery("SELECT p FROM UsStatePowerGenEntity p ORDER BY p.totalGen desc",
                UsStatePowerGenEntity.class).setMaxResults(n).getResultList().stream().sorted(Comparator.comparing(UsStatePowerGenEntity::getTotalGen)).limit(n).map(us->{
           MapMarkerModel map=new MapMarkerModel();
           df.setRoundingMode(RoundingMode.UP);
           map.setPosition(new Position(us.getLat(), us.getLng()));
           Map<String, String> infoMap=new HashMap<>();
           infoMap.put("stateName", us.getName());
           infoMap.put("Total_Generation", df.format(us.getTotalGen()/1000)+"KWh");
           double genPer=((us.getTotalGen()/totalGeneration)*100);
           String generationPercent=df.format(genPer);
           infoMap.put("Tot_Percent", generationPercent+"%");
           map.setInfo(infoMap);
           map.setLabel(new Label("black" , us.getName()));
           map.setTitle("Power Generation Details");
           return map;
        }).collect(Collectors.toList());
    }
    
    public List<MapMarkerModel> findByStateName(String name){
        double totalGeneration= genRepository.findAll().stream().mapToDouble(gen->gen.getTotalGen()).sum();
        if(name!=null && !name.isEmpty()) {
            return genRepository.findByName(name).stream().sorted(Comparator.comparing(UsStatePowerGenEntity::getTotalGen)).map(us->{
                MapMarkerModel map=new MapMarkerModel();
                df.setRoundingMode(RoundingMode.UP);
                map.setPosition(new Position(us.getLat(), us.getLng()));
                Map<String, String> infoMap=new HashMap<>();
                infoMap.put("stateName", us.getName());
                infoMap.put("Total_Generation", df.format(us.getTotalGen()/1000)+"KWh");
                double genPer=((((us.getTotalGen())/totalGeneration)*100));
                String generationPercent=df.format(genPer);
                infoMap.put("Tot_Percent", generationPercent+"%");
                map.setInfo(infoMap);
                map.setLabel(new Label("black" , us.getName()));
                map.setTitle("Power Generation Details");
                return map;
            }).collect(Collectors.toList());
        }
        return null;
    }
    public List<State> getStates(){
        return genRepository.findAll().stream().map(us->new State(us.getName())).collect(Collectors.toList());
    }
}
